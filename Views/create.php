<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="post">
            <fieldset>
                <legend>Add Quantity</legend>
                
                <div>
                    <label for="date_name">Enter Weekday</label>
                    <input autofocus="autofocus"                     
                           placeholder="weekday" 
                           type="text" 
                           name="day_name"                     
                           required="required"   
                           />                                            
                 </div>
                <div>
                    <label for="date">Enter Date</label>
                    <input placeholder="date" 
                           type="date" 
                           name="day_date"
                           required="required"   
                           />                   
                </div>
                <div>
                    <label for="manager">Enter Manager</label>
                    <input placeholder="manager" 
                           type="text" 
                           name="manager"
                           required="required"   
                           />                   
                </div>
                <div>
                    <label for="qty">Enter Quantity</label>
                    <input placeholder="quantity" 
                           type="text" 
                           name="qty"
                           required="required"   
                           />                   
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>




				